# course-mlops-v3

[course-mlops-v3](https://ods.ai/tracks/mlops3-course-spring-2024)


## Repository Methodology

1. each experiment is a separate branch
2. If you want to use anything from other experiments then clone branch content
3. master contains the best experiment
4. all experiments should be reproducible
5. any manual manipulations should be put into bash script

# How to use linters in this project


## install & init pre commit
```bash
pip install pre-commit
pre-commit install
```

## run pre commit

create `example.py` as example: `echo "import pandas as pd\n\ndf = pd.DataFrame()  \n\nprint('fix it ')   \nimport os\nos.exit(0)" > example.py`

run pre commit hooks on commit automatically

```bash
git add example.py
git commit -m "$COMMIT_MESSAGE"
```

or manually

```bash
pre-commit run -a
# or
pre-commit run --files example.py
```
